import { actionTypes } from "../actions";

export default (state = false, action) => {
  switch (action.type) {
    case actionTypes.CORRECT_GUESS:
      return true;
    case actionTypes.INCORRECT_GUESS:
      return false;
    default:
      return state;
  }
};
