import success from "./successReducer";
import guess from "./guessWordReducer";
import secretWord from "./secretWordReducer";
import { combineReducers } from "redux";

export default combineReducers({ success, guessedWords: guess, secretWord });
