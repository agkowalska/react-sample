import { storeFactory } from "../test/testUtils";
import App from "./App";
import React from "react";
import { shallow } from "enzyme";

const setup = (state = {}) => {
  const store = storeFactory(state);
  return shallow(<App store={store} />).dive();
};

describe("verify redux properites", () => {
  test("check success property", () => {
    const success = true;
    const wrapper = setup({ success });

    const successProp = wrapper.props().success;

    expect(successProp).toBe(success);
  });

  test("check secretWord property", () => {
    const secretWord = "party";
    const wrapper = setup({ secretWord });
    const successProp = wrapper.props().secretWord;

    expect(successProp).toBe(secretWord);
  });

  test("check guessedWords property", () => {
    const guessedWords = [{ guessedWord: "train", letterMatchCount: 3 }];
    const wrapper = setup({ guessedWords });
    const successProp = wrapper.props().guessedWords;

    expect(successProp).toBe(guessedWords);
  });

  test("check guessedWord function property", () => {
    const wrapper = setup();
    const guessWord = wrapper.props().guessWord;

    expect(guessWord).toBeInstanceOf(Function);
  });

  test("check getSecretWord function property", () => {
    const wrapper = setup();
    const getSecretWord = wrapper.props().getSecretWord;

    expect(getSecretWord).toBeInstanceOf(Function);
  });
});
