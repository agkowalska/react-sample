import React from "react";
import "./App.scss";
import { Congrats } from "./components/jotto/congrats/Congrats";
import { GuessedWords } from "./components/jotto/guessed-words/GuessedWords";
import Input from "./components/input/Input";
import { connect } from "react-redux";

const guessedWords = [];

function App() {
  return (
    <div className="App">
      <h1> Jotto </h1>
      <Congrats message={true} />
      <Input />
      <GuessedWords guessedWords={guessedWords} />
    </div>
  );
}

const mapStateToProps = (state) => {
  const { success, guessedWords, secretWord } = state;
  return { success, guessedWords, secretWord };
};

const mapDispatchToProps = (dispatch) => ({
  guessWord: () => dispatch.guessWord,
  getSecretWord: () => dispatch.getSecretWord,
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
