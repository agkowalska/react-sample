import React, { useEffect, useState } from "react";
import { Transaction } from "../transaction/Transaction";
import { transactionsJSON } from "../data/transactions";
import "./transactions.scss";

const apiURL = "localhost:3000/";
const transaction = `${apiURL}transactions`;

export const Transactions = () => {
  const [transactions, setTransactions] = useState([]);

  useEffect(() => {
    // (async function anyNameFunction() {
    //     const allTransactions = await http.get(transactionURL);
    //
    //
    // })();

    setTransactions(transactionsJSON);
  }, []);

  const displayNoTransaction = () => <p> No transactions found </p>;

  return (
    <div className="transaction-container">
      {!transactions.length && displayNoTransaction()}
      {transactions.length &&
        transactions.map((transaction, index) => (
          <Transaction key={index} {...transaction} />
        ))}
    </div>
  );
};
