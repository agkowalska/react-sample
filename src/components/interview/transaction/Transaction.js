import React from "react";
import "./transaction.scss";
import moment from "moment";

export const Transaction = ({ clearings, authorization }) => {
  // move to dedcated service
  const formatDate = (date) => {
    return moment(date).format("MMM Do YYYY");
  };

  const renderLocation = ({ city, country }) => (
    <>
      {city} <span> {country}</span>{" "}
    </>
  );

  const renderLocationDetails = ({ date, amount, location }, key) => {
    return (
      <div key={key} className="transaction__content">
        <div className="transaction__info-box">
          <p className="transaction__title"> Date </p>
          {formatDate(date)}{" "}
        </div>
        <div className="transaction__info-box">
          <p className="transaction__title"> Amount </p>
          {amount}{" "}
        </div>
        <div className="transaction__info-box">
          <p className="transaction__title"> Location </p>
          {location ? renderLocation(location) : <p> N/A </p>}
        </div>
      </div>
    );
  };

  const getTransactionDetails = () => {
    return clearings.length
      ? clearings.map((clearing, index) =>
          renderLocationDetails(clearing, index)
        )
      : renderLocationDetails(authorization, 0);
  };

  return <div className="transaction">{getTransactionDetails()}</div>;
};
