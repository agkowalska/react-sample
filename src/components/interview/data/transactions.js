export const transactionsJSON = [
  {
    authorization: {
      amount: "CHF 100.56",
      date: "2020-02-20",
      location: {
        city: "Zurich",
        country: "CHE",
      },
    },
    clearings: [
      {
        amount: "CHF 50.00",
        date: "2020-02-20",
      },
      {
        amount: "CHF 50.56",
        date: "2020-02-27",
      },
      {
        amount: "CHF 100.56",
        date: "2020-03-20",
      },
    ],
  },
  {
    authorization: {
      amount: "EUR 234.99",
      date: "2020-03-05",
      location: {
        city: "Berlin",
        country: "DEU",
      },
    },
    clearings: [
      {
        amount: "EUR 234.99",
        date: "2020-03-05",
      },
    ],
  },
  {
    authorization: null,
    clearings: [
      {
        amount: "CHF 20.45",
        date: "2020-03-15",
      },
    ],
  },
  {
    authorization: null,
    clearings: [
      {
        amount: "CHF 1600.00",
        date: "2020-03-15",
      },
    ],
  },
  {
    authorization: {
      amount: "USD 856.75",
      date: "2020-03-30",
      location: {
        city: "Los Angeles",
        country: "USA",
      },
    },
    clearings: [],
  },
  {
    authorization: {
      amount: "CHF 211.00",
      date: "2020-04-01",
      location: {
        city: "Zurich",
        country: "CHE",
      },
    },
    clearings: [
      {
        amount: "CHF 211.00",
        date: "2020-04-02",
      },
    ],
  },
];
