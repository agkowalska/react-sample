import React from "react";
import { connect } from "react-redux";
import { guessWord } from "../../actions";

class Input extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const contents = this.props.success ? null : (
      <form className="form-inline">
        <input
          data-test="input-box"
          className="mb-2 form-control"
          type="text"
          placeholder="enter guess"
        />
        <button
          data-test="submit-button"
          className="btn btn-primary mx-sm-3 mb-2"
          type="submit"
        >
          Submit
        </button>
      </form>
    );
    return <div data-test="component-input">{contents}</div>;
  }
}

const mapStateToProps = ({ success }) => ({ success });

const mapDispatchToProps = (dispatch) => ({
  guessWord: () => dispatch.guessWord,
});

export default connect(mapStateToProps, mapDispatchToProps)(Input);
