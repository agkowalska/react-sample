import React from "react";
import { render, shallow } from "enzyme";

import { GuessedWords } from "./GuessedWords";
import { findByTestAttribute } from "../../../../test/testUtils";

const defaultProps = {
  guessedWords: [{ guessedWord: "train", letterMatchCount: 3 }],
};

const setup = (props = {}, state = {}) => {
  const setupProps = { ...defaultProps, ...props };
  // https://gist.github.com/fokusferit/e4558d384e4e9cab95d04e5f35d4f913
  return shallow(<GuessedWords {...setupProps} />);
};

describe("if there are no words guessed", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = setup({ guessedWords: [] });
  });

  test("renders without error", () => {
    const component = findByTestAttribute(wrapper, "component-guessed-words");

    expect(component.length).toBe(1);
  });

  test("renders instruction to guess a word", () => {
    const instructionWrapper = findByTestAttribute(
      wrapper,
      "component-guessed-words-instructions"
    );

    expect(instructionWrapper.text().length).not.toBe(0);
  });
});

describe("if there are words guessed", () => {
  const guessedWords = [
    { guessedWord: "train", letterMatchCount: 3 },
    { guessedWord: "party", letterMatchCount: 1 },
    { guessedWord: "eat", letterMatchCount: 2 },
  ];
  let wrapper;
  beforeEach(() => {
    wrapper = setup({
      guessedWords,
    });
  });

  test("renders without error", () => {
    const component = findByTestAttribute(wrapper, "component-guessed-words");

    expect(component.length).toBe(1);
  });

  test("renders guessed words section", () => {
    const section = findByTestAttribute(
      wrapper,
      "component-guessed-words-section"
    );

    expect(section.length).toBe(1);
  });

  test("correct number of guessed words", () => {
    const guessedWordsNodes = findByTestAttribute(wrapper, "guessed-word");

    expect(guessedWordsNodes.length).toBe(guessedWords.length);
  });
});
