import React from "react";
import PropTypes from "prop-types";

const renderGuessedWords = (arrOfWords) => {
  return arrOfWords.map((guessedWord, index) => (
    <div key={index} data-test="guessed-word">
      {" "}
      {guessedWord.guessedWord} {guessedWord.letterMatchCount}{" "}
    </div>
  ));
};
export const GuessedWords = ({ guessedWords }) => {
  return (
    <div data-test="component-guessed-words">
      {guessedWords.length > 0 && (
        <div data-test="component-guessed-words-section">
          {renderGuessedWords(guessedWords)}
        </div>
      )}
      {guessedWords.length === 0 && (
        <div>
          <div data-test="component-guessed-words-instructions">
            Try to guess a word!
          </div>
        </div>
      )}
    </div>
  );
};

GuessedWords.propTypes = {
  guessedWords: PropTypes.arrayOf(
    PropTypes.shape({
      guessedWord: PropTypes.string,
      letterMatchCount: PropTypes.number,
    })
  ).isRequired,
};
