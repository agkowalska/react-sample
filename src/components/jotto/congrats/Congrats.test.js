import React from "react";
import { Congrats } from "./Congrats";
import { shallow } from "enzyme";
import { checkProps, findByTestAttribute } from "../../../../test/testUtils";

const defaultProps = { message: false };

const setup = (props = {}, state = {}) => {
  const setUpProps = { ...defaultProps, ...props };
  return shallow(<Congrats {...setUpProps} />);
};
test("renders without error", () => {
  const wrapper = setup();
  const component = findByTestAttribute(wrapper, "component-congrats");
  expect(component.length).toBe(0);
});

test("render no text, when message prop is false", () => {
  const wrapper = setup();
  const component = findByTestAttribute(wrapper, "component-congrats");
  expect(component.length).toBe(0);
});

test("render non-empty message, when message prop is true", () => {
  const wrapper = setup({ message: true });
  const component = findByTestAttribute(wrapper, "component-congrats");

  expect(component.length).toBe(1);
});

test("does not throw warning with expected props", () => {
  checkProps(Congrats, defaultProps);
});
