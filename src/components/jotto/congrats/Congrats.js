import React from "react";
import PropTypes from "prop-types";

export const Congrats = ({ message }) => {
  if (message)
    return (
      <div data-test="component-congrats">Congrats! You guessed a word!</div>
    );

  return null;
};

Congrats.propTypes = {
  message: PropTypes.bool.isRequired,
};
