import React from "react";

class Counter extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      counter: 0,
      errorMessage: false,
    };
  }

  handleCounter = (type) => {
    switch (type) {
      case "INCREMENT":
        this.setState((prevState) => ({
          counter: prevState.counter + 1,
          errorMessage: false,
        }));
        break;
      case "DECREMENT":
        this.setState((prevState) => {
          return prevState.counter > 0
            ? { counter: prevState.counter - 1 }
            : { ...prevState, errorMessage: true };
        });
        break;
      default:
        console.log("nothing specified");
    }
  };

  render() {
    return (
      <div data-test="counter-component">
        <div data-test="counter-component-display">
          The count has been incremented {this.state.counter} times.
        </div>
        {this.state.errorMessage && (
          <div data-test="counter-component-error">
            Oh, you cannot decrement your counter below 0
          </div>
        )}
        <button
          data-test="counter-component-increment"
          onClick={() => this.handleCounter("INCREMENT")}
        >
          Increment counter
        </button>

        <button
          data-test="counter-component-decrement"
          onClick={() => this.handleCounter("DECREMENT")}
        >
          Decrement counter
        </button>
      </div>
    );
  }
}

export default Counter;
