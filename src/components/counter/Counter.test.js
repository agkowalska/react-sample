import React from "react";
import { shallow } from "enzyme";
import Counter from "./Counter";
import { findByTestAttribute } from "../../../test/testUtils";

const setup = (props = {}, state = {}) => {
  const wrapper = shallow(<Counter {...props} />);

  if (state && state.counter) {
    wrapper.setState({ counter: state.counter });
  }

  return wrapper;
};

test("renders without errors", () => {
  const wrapper = setup();
  const component = findByTestAttribute(wrapper, "counter-component");

  expect(component.length).toBe(1);
});

test("renders counter display", () => {
  const wrapper = setup();
  const displayMessage = findByTestAttribute(
    wrapper,
    "counter-component-display"
  );
  expect(displayMessage.text()).toContain(0);
});

test("renders increment button", () => {
  const wrapper = setup();
  const button = findByTestAttribute(wrapper, "counter-component-increment");

  expect(button.length).toBe(1);
});

test("counter start at 0", () => {
  const wrapper = setup();
  const initialStateCounter = wrapper.state("counter");
  expect(initialStateCounter).toBe(0);
});

test("clicking button increments counter", () => {
  const counter = 7;
  const wrapper = setup(null, { counter });

  const button = findByTestAttribute(wrapper, "counter-component-increment");

  button.simulate("click");

  const displayMessage = findByTestAttribute(
    wrapper,
    "counter-component-display"
  );

  expect(displayMessage.text()).toContain(counter + 1);
});

test("renders decrement button", () => {
  const wrapper = setup();

  const button = findByTestAttribute(wrapper, "counter-component-decrement");

  expect(button.length).toBe(1);
});

test("verify decrement button working", () => {
  const counter = 8;
  const wrapper = setup(null, { counter });

  const button = findByTestAttribute(wrapper, "counter-component-decrement");

  button.simulate("click");

  const displayMessage = findByTestAttribute(
    wrapper,
    "counter-component-display"
  );

  expect(displayMessage.text()).toContain(counter - 1);
});

test("verify that counter cannot be decrement below the zero", () => {
  const counter = 0;

  const wrapper = setup({ counter });

  const button = findByTestAttribute(wrapper, "counter-component-decrement");

  button.simulate("click");

  const errorMessage = findByTestAttribute(wrapper, "counter-component-error");

  expect(errorMessage.length).toBe(1);
});

test("verify showing and hiding error message", () => {
  const counter = 0;

  const wrapper = setup({ counter });

  const decrementButton = findByTestAttribute(
    wrapper,
    "counter-component-decrement"
  );

  decrementButton.simulate("click");

  const errorMessage = findByTestAttribute(wrapper, "counter-component-error");

  expect(errorMessage.length).toBe(1);

  const incrementButton = findByTestAttribute(
    wrapper,
    "counter-component-increment"
  );

  incrementButton.simulate("click");

  const anotherErrorMessage = findByTestAttribute(
    wrapper,
    "counter-component-error"
  );

  expect(anotherErrorMessage.length).toBe(0);
});
