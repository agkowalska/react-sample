import { getLetterMatchCount } from "./index";

describe("getLetterMatchCount", () => {
  let secretWord = "";
  beforeEach(() => {
    secretWord = "party";
  });
  test("return correct count when there are no matching letter", () => {
    const guessedWord = "bones";
    const letterMatchCount = getLetterMatchCount(guessedWord, secretWord);
    expect(letterMatchCount).toBe(0);
  });

  test("rerun the correct count where the are 4 matching letter", () => {
    const guessedWord = "parties";
    const letterMatchCount = getLetterMatchCount(guessedWord, secretWord);
    expect(letterMatchCount).toBe(4);
  });

  test("return the correct number of matching letters when there are duplicate letters in the guess", () => {
    const guessedWord = "parka";
    const letterMatchCount = getLetterMatchCount(guessedWord, secretWord);
    expect(letterMatchCount).toBe(3);
  });
});
