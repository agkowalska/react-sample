import checkPropTypes from "check-prop-types";

import rootReducer from "../src/reducers/index";
import {applyMiddleware, createStore} from 'redux';
import ReduxThunk from 'redux-thunk';


export const storeFactory = initialState => {
    const createStoreWithMiddleware = applyMiddleware(ReduxThunk)(createStore)
    return createStoreWithMiddleware(rootReducer, initialState);
};

export const findByTestAttribute = (wrapper, attribute) => {
    return wrapper.find(`[data-test='${attribute}']`);
}

export const checkProps = (component, conformingProps) => {
    const propError = checkPropTypes(component.propTypes, conformingProps, 'prop', component.name, null);

    expect(propError).toBeUndefined();
}